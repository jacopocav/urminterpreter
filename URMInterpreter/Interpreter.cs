﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace URMInterpreter {
    public class Interpreter {
        public static Registers Execute(Registers registers, List<Instruction> program, bool verbose = false, int startInst = 0) {
            int currentInst = startInst;
            int iteration = 1;
            Instruction inst;

            if (verbose) {
                Console.WriteLine("Input: {0}", registers);
                Console.WriteLine("Instructions: ");
                int line = 1;
                foreach (Instruction ins in program) {
                    Console.WriteLine("{0}: {1}", line++, ins);
                }
                Console.WriteLine("Execution:");
                Console.WriteLine("Iteration#: Instruction# -> Registers");
            }

            while (currentInst < program.Count()) {
                inst = program[currentInst];
                int index1 = Convert.ToInt32(inst[0]);
                int index2 = Convert.ToInt32(inst[1]);
                int index3 = Convert.ToInt32(inst[2]);
                switch (inst.InstrType) {
                    case (Instruction.Type.Z):
                        registers[index1] = 0;
                        break;
                    case (Instruction.Type.S):
                        registers[index1]++;
                        break;
                    case (Instruction.Type.T):
                        registers[index1] = registers[index2];
                        break;
                    case (Instruction.Type.J):
                        if (registers[index1] == registers[index2]) {
                            if (verbose) {
                                Console.WriteLine("{0}: I[{1}] -> {2}", iteration++, currentInst + 1, registers);
                            }
                            currentInst = index3 - 1;
                            continue;
                        }
                        break;
                    default:
                        throw new Exception("Unknown command found: " + inst.InstrType);
                }
                if (verbose) {
                    Console.WriteLine("{0}: I[{1}] -> {2}", iteration++, currentInst + 1, registers);
                }
                currentInst++;
            }
            if (verbose) {
                Console.WriteLine("Output: {0}", registers);
            }

            return registers;
        }
    }
}
