﻿namespace URMInterpreter {
    class InteractiveInstruction : Instruction {
        public string Label { get; }

        public InteractiveInstruction(Instruction.Type type, uint[] pars, string label = null) : base(type, pars) {
            if (label != null) pars[2] = 0;
            Label = label;
        }
    }
}
