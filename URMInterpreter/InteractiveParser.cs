﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace URMInterpreter {
    internal class InteractiveParser {
        private uint _endIndex = 1;

        public Program Program { get; } = new Program(new Registers(0), new List<Instruction>());

        public InteractiveInstruction ParseInstruction(string inst, string pars, string label = null) {
            var type = (Instruction.Type) Enum.Parse(typeof (Instruction.Type), inst);
            var strParams = pars.Split(',');
            uint[] numParams = {0, 0, 0};

            try {
                numParams[0] = uint.Parse(strParams[0]);
                if (strParams.Length > 1) {
                    numParams[1] = uint.Parse(strParams[1]);
                }
            } catch (FormatException) {
                throw new ParseException(-1, "First or second parameters of instruction are not integers");
            }

            InteractiveInstruction interInst;
            if (strParams.Length == 3) {
                uint param3;
                if (!uint.TryParse(strParams[2], out param3)) {
                    interInst = new InteractiveInstruction(type, numParams, strParams[2]);
                } else {
                    numParams[2] = uint.Parse(strParams[2]);
                    interInst = new InteractiveInstruction(type, numParams);
                }
            } else {
                interInst = new InteractiveInstruction(type, numParams);
            }

            _endIndex = Math.Max(_endIndex, Math.Max(numParams[0], numParams[1]));
            return interInst;
        }
    }
}