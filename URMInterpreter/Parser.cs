﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace URMInterpreter {
    public class Parser {
        public static readonly Regex RegisterRegex = new Regex(@"\[\d+(,\d+)*\]");
        public static readonly Regex InstrRegex = new Regex(@"^((?<label>[A-Z0-9]+):)?(?<inst>[ZSTJ])\((?<params>\d+(,\d+(,[A-Z0-9]+)?)?)\)$", RegexOptions.IgnoreCase);

        public static Program ParseFile(string filePath, Registers inputRegs = null) {
            string[] lines = System.IO.File.ReadAllLines(filePath);

            // Removing all whitespace from file
            for (int l = 0; l < lines.Count(); ++l) {
                lines[l] = RemoveWhitespace(lines[l]);
            }

            int instrStartIndex = 1;
            Registers registers;

            // First line: registers input
            if (inputRegs != null) {
                registers = inputRegs;
                if(!RegisterRegex.Match(lines[0]).Success)
                    instrStartIndex = 0;
            } else {
                if (!RegisterRegex.Match(lines[0]).Success) {
                    throw new ParseException(1, "Registers input missing. Insert them in source or as program arguments.");
                }
                string strRegs = lines[0].Remove(0, 1);
                strRegs = strRegs.Remove(strRegs.Length - 1);
                int[] regs = strRegs.Split(',').Select(int.Parse).ToArray();

                 registers = new Registers(regs);
            }

            // Subsequent lines: instructions
            // Label search
            var labels = new Dictionary<string, int>();
            var matches = new List<Match>();
            for (int l = instrStartIndex; l < lines.Count(); ++l) {
                Match m = InstrRegex.Match(lines[l]);

                if (!m.Success) { // Parsing has failed
                    throw new ParseException(l + 1, "Instruction not well formed. Correct format is: \"LABEL: [Z,S,T,J](parameters)");
                }

                if (m.Groups["label"].Success) {
                    if (m.Groups["labels"].Value == "END") {
                        throw new ParseException(l + 1, "Label END is reserved and cannot be used.");
                    }

                    try {
                        labels.Add(m.Groups["label"].Value, l + 1 - instrStartIndex);
                    } catch (ArgumentException) {
                        throw new ParseException(l + 1, "Label " + m.Groups["label"].Value + " has been used more than once.");
                    }
                }
                matches.Add(m);
            }
            labels.Add("END", lines.Count() + 1 - instrStartIndex);

            Instruction[] instructions = new Instruction[lines.Count() - instrStartIndex];
            uint maxRegIndex = 0;

            for (int l = instrStartIndex; l < lines.Count(); ++l) {
                Instruction.Type type = (Instruction.Type)Enum.Parse(typeof(Instruction.Type), matches[l - instrStartIndex].Groups["inst"].Value);
                string[] strParams = matches[l - instrStartIndex].Groups["params"].Value.Split(',');
                uint[] numParams = { 0, 0, 0 };

                try {
                    numParams[0] = uint.Parse(strParams[0]);
                    if (strParams.Length > 1) numParams[1] = uint.Parse(strParams[1]);
                } catch (FormatException) {
                    throw new ParseException(l + 1, "First or second parameters are not integers");
                }

                if (strParams.Length == 3) {
                    int labelLine;
                    if (labels.TryGetValue(strParams[2], out labelLine)) {
                        numParams[2] = Convert.ToUInt32(labelLine);
                    } else {
                        try {
                            numParams[2] = uint.Parse(strParams[2]);
                        } catch (FormatException) {
                            throw new ParseException(l + 1, "Third parameter is not an integer nor a valid label");
                        }
                    }
                }
                instructions[l - instrStartIndex] = new Instruction(type, numParams);

                maxRegIndex = Math.Max(maxRegIndex, Math.Max(numParams[0], numParams[1]));
            }
            if (maxRegIndex > registers.Count) {
                registers[Convert.ToInt32(maxRegIndex)] = 0;
            }

            return new Program(registers, instructions.ToList());
        }

        public static string RemoveWhitespace(string input) => new string(input.Where(c => !char.IsWhiteSpace(c)).ToArray());

    }

    [Serializable]
    public class ParseException : Exception {
        private readonly int line;
        public ParseException(int line, string msg) : base(msg) {
            this.line = line;
        }

        public override string ToString() => "Parse Error" + (line > 0 ? " at line " + line : "") + ": " + Message;
    }
}
