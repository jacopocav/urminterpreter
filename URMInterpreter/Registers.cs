﻿using System.Collections.Generic;
using System.Linq;

namespace URMInterpreter {
    public class Registers {
        private static readonly int DefaultRegCount = 10;

        private List<int> registers;
        public int this[int index] {
            get {
                if (index > registers.Count()) {
                    AddRegister(index - 1);
                }
                return registers[index - 1];
            }
            set {
                if (index > registers.Count()) {
                    AddRegister(index - 1, value);
                } else {
                    registers[index - 1] = value;
                }
            }
        }

        public Registers(params int[] pars) {
            registers = pars.ToList();
        }

        public int Count => registers.Count;

        public Registers() {
            registers = Enumerable.Repeat(0, DefaultRegCount).ToList();
        }

        private void AddRegister(int maxIndex, int value = 0) {
            for (int i = registers.Count - 1; i < maxIndex - 1 ; ++i) {
                registers.Add(0);
            }
            registers.Add(value);
        }

        public override string ToString() {
            return "[" + string.Join(" ", registers) + "]";
        }
    }
}
