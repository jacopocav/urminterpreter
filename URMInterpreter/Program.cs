﻿using System.Collections.Generic;

namespace URMInterpreter {
    public class Program {
        public Registers Registers { get; set; }
        public List<Instruction> Instructions { get; set; }

        public Program(Registers registers, List<Instruction> instructions) {
            Registers = registers;
            Instructions = instructions;
        }

        public Registers Execute(bool verbose = false, int startInst = 0) => Interpreter.Execute(Registers, Instructions, verbose, startInst);
        public Instruction LastInstruction => Instructions[Instructions.Count - 1];
    }
}
