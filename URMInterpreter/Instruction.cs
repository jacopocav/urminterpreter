﻿using System.Linq;
using System.Text;

namespace URMInterpreter {
    /// <summary>
    /// This class models a URM instruction. It holds a type and 3 integer parameters.
    /// </summary>
    public class Instruction {
        private const int MaxParams = 3;

        /// <summary>
        /// The types of URM instructions that can be represented. The types are Z (Zero), S (Successor), T (Copy), J (Jump).
        /// </summary>
        public enum Type {
            Z,
            S,
            T,
            J
        }

        /// <summary>
        /// The current type of URM instruction.
        /// </summary>
        public Type InstrType { get; }

        /// <summary>
        /// Array containing the 3 parameters of the instruction. Unused parameters are set to 0.
        /// </summary>
        private uint[] pars;

        /// <summary>
        /// Indexer for the class. It returns the parameter with index n.
        /// </summary>
        /// <param name="index">Index of the parameter to return</param>
        /// <returns>A copy of the parameter</returns>
        public virtual uint this[int index] => pars[index];

        /// <summary>
        /// Constructor of this class
        /// </summary>
        /// <param name="type">Type of instruction to build</param>
        /// <param name="pars">Parameters of the instruction. They can be inserted as variadic arguments or as an array of int</param>
        public Instruction(Type type, params uint[] pars) {
            InstrType = type;
            this.pars = new uint[MaxParams];
            for (int i = 0; i < pars.Count() && i < MaxParams; ++i) {
                this.pars[i] = pars[i];
            }
        }

        /// <summary>
        /// Converts the instruction to its string representation, with the correct number of parameters.
        /// S and Z have 1 parameter.
        /// T has 2 parameters.
        /// J has 3 parameters.
        /// </summary>
        /// <returns>String representation of the instruction (eg: "J(1,2,3)").</returns>
        public override string ToString() {
            StringBuilder text = new StringBuilder(InstrType.ToString() + "(");
            text.Append(pars[0]);

            if(InstrType == Type.T || InstrType == Type.J) {
                text.Append(",").Append(pars[1]);
                if(InstrType == Type.J) {
                    text.Append(",").Append(pars[2]);
                }
            }
            
            text.Append(")");
            return text.ToString();
        }
    }
}
