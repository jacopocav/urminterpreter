﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace URMInterpreter {
    /// <summary>
    /// Class that contains the logic for command line execution
    /// </summary>
    /// <remarks>
    /// The interpreter can be executed with up to 2 arguments:
    /// - The verbose option "-v" [optional, must be the first argument if present]
    /// - The path to the file to interpret [obligatory]
    /// - The values of the registers, ordered by register number (eg: 5 6 means r1 = 5, r2 = 6) [optional, must be after the file path]
    /// 
    /// The verbose option will print to console the program input, instructions, and results for every instruction performed.
    /// The structure of a URM source file must be according to the following:
    /// - First line: Initial state of the registers, in the form: [n, n, n], where n is any integer (there can be as many ns as needed).
    ///     * If the user passes register values by argument, this line is ignored (and the parser won't produce an error if it's not present)
    ///     * Any register not defined in this line is assumed to have a value of 0.
    ///     * The program can access any register not declared in this line
    /// - Subsequent lines: instructions that make up the program, one for each line, without any separator.
    ///     * Labels can be used to make the code more readable: Just prepend a label to an instruction, separated by a semicolon (eg: "LOOP: S(3)"),
    ///         and then use the label in jump instructions as desired (eg: "J(1,1,LOOP)"). 
    ///     * The END label is reserved and points to the first non-existing instruction of the program.
    ///         Jumping to END will terminate the program
    ///         The END label can always be used in jump instructions, but must not be declared like every other label (otherwise it will produce an error).
    ///     * Labels are internally converted to line numbers, so the parsed code is URM compliant.
    /// 
    /// If no arguments are passed, the interpreter will start interactive mode, a REPL console where the user can input URM instructions and execute them in real time.
    /// [TODO INSERIRE DESCRIZIONE FUNZIONAMENTO REPL]
    /// 
    /// Please note that ALL indexes (registers, line numbers) start from 1, not 0.
    /// </remarks>
    public class MainClass {

        /// <summary>
        /// Regular expression for the :do(I(params), t) command
        /// </summary>
        private static readonly Regex RepeatRegex =
            new Regex(@"^:do\((?<inst>[ZSTJ])\((?<params>\d+(,\d+(,\d+)?)?)\),(?<times>\d+)\)$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex for the :set(r, v) command
        /// </summary>
        private static readonly Regex SetSingleRegisterRegex = new Regex(@"^:set\((?<reg>\d+),(?<val>\d+)\)$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex for the :set[v1, v2, ...] command
        /// </summary>
        private static readonly Regex SetRegistersRegex = new Regex(@"^:set\[(?<vals>\d+(,\d+)*)\]$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex for the :goto(i) command
        /// </summary>
        private static readonly Regex GotoRegex = new Regex(@"^:goto\((?<inst>\d+)\)$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex for any URM instruction
        /// </summary>
        private static readonly Regex InteractiveInstrRegex = new Regex(@"^(?<inst>[ZSTJ])\((?<params>\d+(,\d+(,\d+)?)?)\)$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Main of this project.
        /// </summary>
        /// <param name="args">The arguments passed when the executable is run from a console. The order is irrelevant.</param>
        public static void Main(string[] args) {

            string file;
            bool verbose = false;
            Registers regs = null;

            if (args.Length == 0) { // Start interactive mode
                InteractiveLoop();
                return;
            }
            if (args.Length < 2) { // Only file path has been passed
                file = args[0];
            } else {
                int regIndex;
                if (args[0] == "-v") {
                    verbose = true;
                    file = args[1];
                    regIndex = 2;
                } else {
                    file = args[0];
                    regIndex = 1;
                }
                if (args.Length > regIndex) { // The user has specified input registers
                    int[] registers = new int[args.Length - regIndex];
                    for (int i = regIndex; i < args.Length; ++i) {
                        registers[i - regIndex] = Math.Abs(int.Parse(args[i]));
                    }
                    regs = new Registers(registers);
                }
            }

            try {
                Program prog = Parser.ParseFile(file, regs);
                prog.Execute(verbose);
                if (!verbose) { // With verbose = true, the output has already been printed
                    Console.WriteLine("Output: {0}", prog.Registers);
                }
            } catch (ParseException pe) {
                Console.WriteLine(pe);
            } catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// This function controls interactive console execution
        /// </summary>
        private static void InteractiveLoop() {
            // Welcome message
            Console.WriteLine(string.Join(Environment.NewLine, 
                "URMI - Unlimited Registers Machine Interpreter, by Jacopo Cavallarin",
                "Interactive mode: Labels are not supported",
                "Live execution enabled",
                "Type \":help\" for more info on commands.",
                ""));

            var line = ""; // Line read by input
            var intParser = new InteractiveParser();
            var nextIndex = 0; // Index of next instruction to execute
            var verbose = true;
            var live = true;

            while (line != ":q") {
                try {
                    // Reading input
                    Console.Write(nextIndex + "|" + intParser.Program.Instructions.Count +"> ");
                    line = Parser.RemoveWhitespace(Console.ReadLine());

                    // Checking for special commands
                    if (line.StartsWith(":")) {
                        switch (line) {
                            case ":q": // Quit console
                                return;
                            case ":help": // Print all available instructions
                                Console.WriteLine(string.Join(Environment.NewLine, 
                                    "",
                                    " n|m>\t\tLast instruction executed | Total instruction count",
                                    "",
                                    " INSTRUCTIONS:",
                                    " S(n)\t\tIncrements register r_n by 1",
                                    " Z(n)\t\tSets r_n to zero",
                                    " T(m,n)\t\tCopies r_n value to r_m",
                                    " J(n,m,i)\tGoes to instruction i if r_n = r_m",
                                    "",
                                    " COMMANDS:",
                                    " :do(I, t)\t\tInsert an instruction I into the program t times",
                                    " :set(n, v)\t\tSets r_n to value v (without adding any instruction)",
                                    " :set[v1,v2,...]\tResets all registers and sets them to values specified",
                                    " :goto(i)\t\tSets instruction i as the next to be executed",
                                    " :printi\t\tPrints all inserted instructions",
                                    " :printr\t\tPrints values of used registers",
                                    " :resetr\t\tResets all registers to zero",
                                    " :reseti\t\tRemoves all instructions",
                                    " :pause\t\t\tTurns live instruction execution off",
                                    " :run\t\t\tResumes execution from the instruction following the last executed",
                                    " :verbose\t\tToggle verbose output (prints register values in live mode)",
                                    " :q\t\t\tQuits the program",
                                    ""));
                                continue;
                            case ":printi": // Print all previously inserted instructions
                                Console.WriteLine();
                                for (var i = 0; i < intParser.Program.Instructions.Count; ++i) {
                                    Console.WriteLine(" " + (i + 1) + ") " + intParser.Program.Instructions[i]);
                                }
                                Console.WriteLine();
                                continue;
                            case ":printr": // Print registers
                                PrintRegisters(intParser.Program);
                                continue;
                            case ":resetr": // Reset all registers
                                intParser.Program.Registers = new Registers(0);
                                continue;
                            case ":reseti": // Reset all instructions
                                intParser.Program.Instructions.Clear();
                                nextIndex = 0;
                                continue;
                            case ":pause":
                                live = false;
                                Console.WriteLine(Environment.NewLine + " Live Execution OFF" + Environment.NewLine);
                                continue;
                            case ":run":
                                live = true; 
                                Console.WriteLine(Environment.NewLine + " Resuming execution from instruction #" + (nextIndex + 1));
                                intParser.Program.Execute(false, nextIndex);
                                nextIndex = intParser.Program.Instructions.Count;
                                if (verbose) PrintRegisters(intParser.Program);
                                continue;
                            case ":verbose": // Toggle verbose output on/off
                                verbose = !verbose;
                                Console.WriteLine(Environment.NewLine + " Verbose output " + (verbose ? "ON" : "OFF") + Environment.NewLine);
                                continue;
                        }

                        // Matching :do(i, t) command
                        var match = RepeatRegex.Match(line);
                        if (match.Success) {
                            // Repeate instruction multiple times
                            var inst = match.Groups["inst"].Value;
                            var pars = match.Groups["params"].Value;
                            var times = uint.Parse(match.Groups["times"].Value);

                            Instruction instr = intParser.ParseInstruction(inst, pars);

                            for (var i = 0; i < times; ++i)
                                intParser.Program.Instructions.Add(instr);

                            if (live) {
                                intParser.Program.Execute(false, nextIndex);
                                nextIndex = intParser.Program.Instructions.Count;
                                if (verbose) PrintRegisters(intParser.Program);
                            }
                            continue;
                        }

                        // Matching :set(r, v) command
                        match = SetSingleRegisterRegex.Match(line);
                        if (match.Success) {
                            // Set a certain register to some value
                            var reg = int.Parse(match.Groups["reg"].Value);
                            var val = int.Parse(match.Groups["val"].Value);

                            intParser.Program.Registers[reg] = val;
                            if(verbose) PrintRegisters(intParser.Program);
                            continue;
                        }

                        // Matching :set[v1,v2, ...] command
                        match = SetRegistersRegex.Match(line);
                        if (match.Success) {
                            // Set a certain register to some value
                            var vals = new List<string>(match.Groups["vals"].Value.Split(','));

                            var intVals = vals.Select(int.Parse).ToArray();

                            intParser.Program.Registers = new Registers(intVals);

                            if (verbose) PrintRegisters(intParser.Program);
                            continue;
                        }

                        // Matching :goto(i) command
                        match = GotoRegex.Match(line);
                        if (match.Success) {
                            var instIndex = int.Parse(match.Groups["inst"].Value);
                            if(instIndex < 1 || instIndex > intParser.Program.Instructions.Count)
                                throw new Exception("Instruction index out of bounds");
                            nextIndex = instIndex - 1;
                            Console.WriteLine(Environment.NewLine + " Next instruction is #" + (nextIndex + 1) + Environment.NewLine);

                            if (live) {
                                intParser.Program.Execute(false, nextIndex);
                                nextIndex = intParser.Program.Instructions.Count;
                                if (verbose) PrintRegisters(intParser.Program);
                            }
                            
                            continue;
                        }

                    }
                    // Matching URM instruction
                    var instMatch = InteractiveInstrRegex.Match(line);
                    if (!instMatch.Success) {
                        Console.WriteLine(Environment.NewLine + "Invalid command or instruction. Type \":help\" for a list of all instructions." + Environment.NewLine);
                        continue;
                    }

                    var instType = instMatch.Groups["inst"].Value;
                    var param = instMatch.Groups["params"].Value;

                    var intInstr = intParser.ParseInstruction(instType, param);

                    intParser.Program.Instructions.Add(intInstr);

                    if (live) {
                        nextIndex++;
                        intParser.Program.Execute(false, nextIndex - 1);
                        if (verbose) PrintRegisters(intParser.Program);
                    }

                    
                } catch (Exception ex) {
                    Console.WriteLine(Environment.NewLine + " " + ex.Message + Environment.NewLine);
                }
            }
        }

        /// <summary>
        /// Prints register values
        /// </summary>
        /// <param name="program">Program containing the registers to print</param>
        private static void PrintRegisters(Program program) => Console.WriteLine(Environment.NewLine + " " + program.Registers + Environment.NewLine);
    }
}
